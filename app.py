from flask import Flask, request, make_response
from flask_cors import CORS, cross_origin
import pymysql.cursors
import json
from warrant import Cognito

REQUEST_TYPE_FOR_MULT = 'application/json'

USER_POOL_ID = 'us-east-2_hPXEd238g'
USER_POOL_CLIENT = '3qu0plg85g805v4puv6pcv9k48'

NO_TOKEN_NEEDED = ['login', 'forgot_password', 'forgot_password_confirm']


app = Flask(__name__)

CORS(app, send_wildcard=True)

app.config['CORS_HEADERS'] = 'Content-Type'


connection = pymysql.connect(host='aiq-dev.cmda60he27z4.us-east-2.rds.amazonaws.com',
                             user='root',
                             password='A1Q3MAIL',
                             db='metis')



@app.before_request
def reopen_db_connection():
    if not connection.open:
        connection.ping(reconnect=True)

@app.before_request
def check_token_if_needed():
    if request.method != 'OPTIONS' and request.endpoint not in NO_TOKEN_NEEDED:
        if request.headers.get('Authorization', None):
            auth_header = request.headers.get('Authorization')
            if "Bearer" in auth_header:
                try:
                    u = Cognito(USER_POOL_ID, USER_POOL_CLIENT, user_pool_region='us-east-2',
                                access_token=auth_header.split(" ")[1])

                    u.check_token()
                except Exception as e:
                    return ("Please login", str(400))
        else:
            return ("Please login", str(400))


def zip_labels_to_values(desc_obj, values, as_json=True):
    column_names = [k[0] for k in desc_obj]
    full_dict = dict(zip(column_names, values))
    return json.dumps(full_dict, default=str) if as_json else full_dict


def zip_to_multiple_tuples(desc_obj, tuples):
    return json.dumps([zip_labels_to_values(desc_obj, k, False) for k in tuples], default=str)


@app.route('/<user_id>', methods=['GET'])
def get_user(user_id):
    '''

    :param user_id: Integer ID for the user to be fetched.
    :return: JSON formatted user data and successful (200) status code, else 400
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM User WHERE id=%s"
            cursor.execute(sql, (user_id))
        connection.commit()
        return zip_labels_to_values(cursor.description, cursor.fetchone()), str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


# TODO: Switch this to Cognito user pull.
@app.route('/all', methods=['GET'])
def get_all_user():
    '''
    :return: Returns all users, JSON-formatted
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM User"
            cursor.execute(sql)
        connection.commit()
        return zip_to_multiple_tuples(cursor.description, cursor.fetchall()), str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)

@app.route('/login', methods=['POST'])
def login():
    '''
    This will return an access token in the Set-Cookie secure header of the response, if login is valid.
    Takes in Basic Auth in header (i.e. header should have Authorization field with value `Basic base64(<email>:<password>)`)
    :return:
    '''
    if request.headers.get('authorization', None):

        creds = request.authorization
        try:
            u = Cognito(USER_POOL_ID, USER_POOL_CLIENT,
                        username=creds.username, user_pool_region='us-east-2')

            u.authenticate(password=creds.password)
        except Exception as e:
            return "AuthErr: if new acct please change password via confirmation email", str(400)

        return json.dumps({"access_token": u.access_token})
    else:
        return str(400)


@app.route('/register', methods=['POST'])
def register():
    '''
    Takes same auth header as /login, but also requires JSON body with single field {"name": <FirstName LastName>}
    :return:
    '''
    if request.headers.get('authorization', None):
        creds = request.authorization
        body = request.get_json()
        u = Cognito(USER_POOL_ID, USER_POOL_CLIENT, user_pool_region='us-east-2')
        u.add_base_attributes(name=body['name'])

        u.register(creds.username, creds.password)

        return str(200)
    else:
        return str(400)


@app.route('/confirm-registration/<user_name>/<conf_code>', methods=['POST'])
def confirm_register(user_name, conf_code):
    '''
    Confirms a newly registered account. The account is not active previously to this. This link should be given in their confirmation email.
    :param user_name:
    :param conf_code:
    :return:
    '''
    if request.headers.get('authorization', None):
        creds = request.authorization
        body = request.get_json()
        u = Cognito(USER_POOL_ID, USER_POOL_CLIENT, user_pool_region='us-east-2')
        u.confirm_sign_up(conf_code, user_name)

        return str(200)
    else:
        return str(400)


@app.route('/forgot-password', methods=['POST'])
def forgot_password():
    '''
    Expect JSON body with field user_name. Sends email with verification code to user to be used in /forgot-password-confirm
    :return:
    '''
    body = request.get_json()
    if body is None or not body['user_name']:
        return "Expected user_name", str(400)
    try:
        u = Cognito(USER_POOL_ID, USER_POOL_CLIENT, user_pool_region='us-east-2',
                    username=body['user_name'])

        u.initiate_forgot_password()
        
        return str(200)
    except Exception as e:
        return str(400)

@app.route('/forgot-password-confirm/<verify_code>', methods=['POST'])
def forgot_password_confirm(verify_code):
    '''
    Confirms forgot password with verification code. Expects auth header with Basic auth
    :return:
    '''
    body = request.get_json()
    if request.headers.get('authentication', None):
        try:
            u = Cognito(USER_POOL_ID, USER_POOL_CLIENT, user_pool_region='us-east-2',
                        username=request.authorization.username)

            u.confirm_forgot_password(verify_code, request.authorization.password)
            return str(200)
        except:
            return str(400)

@app.route('/remove-user', methods=['DELETE'])
def del_user():
    if request.headers.get('authentication', None):
        u = Cognito(USER_POOL_ID, USER_POOL_CLIENT)
        u.add_base_attributes(email='you@you.com', some_random_attr='random value')

        u.register('username', 'password')

        return str(200)
    else:
        return str(400)



# TODO: Add a join here to get a user's individually approved transactions
@app.route('/user-details/<user_id>', methods=['GET'])
def get_personal_overview(user_id):
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM Users WHERE id=%s"
            cursor.execute(sql, user_id)
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/grants', methods=['GET'])
def get_grant_list():
    '''
    :return: Returns the list of grants which are currently in the db.
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM Grants"
            cursor.execute(sql)
        connection.commit()
        return zip_to_multiple_tuples(cursor.description, cursor.fetchall()), str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/grant-transactions/<grant_id>', methods=['GET'])
def get_grant_trans_by_id(grant_id):
    '''

    :param grant_id: The integer ID of the grant, which has transactions to be fetched.
    :return: JSON-formatted list of transaction objects.
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM Expenses WHERE grant_id=%s"
            cursor.execute(sql, grant_id)
        connection.commit()
        return zip_to_multiple_tuples(cursor.description, cursor.fetchall()), str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


# TODO: Figure out grant metrics...
@app.route('/grant-metrics/<grant_id>', methods=['GET'])
def get_grant_metrics_by_id(grant_id):
    '''
    :param grant_id: The integer ID of the grant metrics used to populate visualizations.
    :return: JSON-formatted metrics.
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT * FROM Expenses WHERE grant_id='%s'"
            cursor.execute(sql)
        connection.commit()
        return zip_to_multiple_tuples(cursor.description, cursor.fetchall()), str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/add-user', methods=['POST'])
def add_new_user():
    '''
    Accepts JSON-formatted user {username: str, admin: bool} in request body.
    :return: 200 if successful, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    user = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO User (user_name, admin) VALUES (%s, %s)"
            cursor.execute(sql,
                           (user['user_name'],
                           user['admin']))
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/delete-user/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    '''
    :param user_id: The integer ID of the user to be deleted.
    :return: 200, else 400
    '''
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM User WHERE id=%s"
            cursor.execute(sql, user_id)
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/update-user', methods=['PUT'])
def update_user():
    '''
    Takes full user object in JSON format {user_name: str, admin: bool}
    :return: 200, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    user_details = request.get_json()
    try:
        with connection.cursor() as cursor:
            # TODO: check for credentials from submitter before updating admin flag.
            sql = "UPDATE User SET user_name=%s, admin=%s WHERE id='%s'"
            cursor.execute(sql, user_details['id'])
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/grant-access/<grant_id>/<user_name>', methods=['PUT'])
def grant_new_access(grant_id, user_name):
    '''
    Grants access to a grant for a specified user in the format
    /grant-access/<grant_id>/<user_name>
    :param grant_id: The integer ID of the grant
    :param user_name: The integer ID of the user
    :return: 200, else 400
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO GrantPermissions (id, user_name) VALUES (%s, %s)"
            cursor.execute(sql, (grant_id, user_name))
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/remove-access/<grant_id>/<user_name>', methods=['DELETE'])
def remove_access(grant_id, user_name):
    '''
    Removes grant access permissions for a given user given the format
    /remove-access/<grant_id>/<user_name>
    :param grant_id: The integer ID of the grant
    :param user_name: The integer ID of the user
    :return: 200, else 400
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "DELETE FROM GrantPermissions WHERE user_name=%s AND grant_id=%s"
            cursor.execute(sql, (user_name, grant_id))
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/add-grant', methods=['POST'])
def add_grant():
    '''
    Accepts a grant object in JSON format as request body.
    :return: 200, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    grant_details = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO Grants (grant_name, total) VALUES (%s, %s)"
            cursor.execute(sql, (grant_details['grant_name'], grant_details['total']))
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/edit-grant', methods=['PUT'])
def edit_grant():
    '''
    Takes a full grant object with updated values in JSON format as request body.
    :return:200, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    grant_details = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "UPDATE Grants SET grant_name=%s, total=%s WHERE id=%s"
            cursor.execute(sql,
                           grant_details['grant_name'],
                           grant_details['total'],
                           grant_details['id'])
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/remove-grant/<grant_id>', methods=['DELETE'])
def remove_grant(grant_id):
    '''
    Permanently deletes a grant object. Historical transactions and grant permissions remain and can be restored if grant is re-added.
    Given in the format:
    /remove-grant/<grant_id>
    :param grant_id: The integer ID of the grant to be deleted
    :return:
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "DELETE FROM Grants WHERE id=%s"
            cursor.execute(sql, grant_id)
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/new-expense', methods=['POST'])
def add_expense():
    '''
    The endpoint to submit a new expense for a given grant. Accept JSON formatted expense object {grant_id:int, reason:str, paid_to: str, date_paid:date, amount:float, misc_note:str, authorizer_id:int}
    :return: 200, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    expense_details = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO Expenses (grant_id, reason, paid_to, date_paid, amount, misc_note, authorizer_id) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql,
                           expense_details['grant_id'],
                           expense_details['reason'],
                           expense_details['paid_to'],
                           expense_details['date_paid'],
                           expense_details['amount'],
                           expense_details['misc_note'],
                           expense_details['authorizer_id'])
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/edit-expense', methods=['PUT'])
def edit_expense():
    '''
    Edit an expense object. Takes a full expense object as JSON in request body {grant_id:int, reason:str, paid_to: str, date_paid:date, amount:float, misc_note:str, authorizer_id:int}
    :return: 200, else 400
    '''
    if request.content_type != REQUEST_TYPE_FOR_MULT:
        raise Exception("Required: JSON format in request body.")
    expense_details = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "UPDATE Expenses SET grant_id=%s, reason=%s, paid_to=%s, date_paid=%s, amount=%s, misc_note=%s, authorizer_id=%s WHERE id=%s"
            cursor.execute(sql,
                           expense_details['grant_id'],
                           expense_details['reason'],
                           expense_details['paid_to'],
                           expense_details['date_paid'],
                           expense_details['amount'],
                           expense_details['misc_note'],
                           expense_details['authorizer_id'])
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


@app.route('/remove-expense/<expense_id>', methods=['DELETE'])
def remove_expense(expense_id):
    '''
    Deletes an expense given the expense's integer ID, give the format
    /remove-expense/<expense_id>
    :param expense_id: The integer ID of the expense to be permanently deleted
    :return: 200, else 400
    '''
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "DELETE FROM Expenses WHERE id=%s"
            cursor.execute(sql, expense_id)
        connection.commit()
        return str(200)
    except Exception as e:
        print(e)
        return "ERROR", str(400)


if __name__ == '__main__':
    app.run(debug=True)



